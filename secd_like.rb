# Syntax
Variable = Struct.new(:index)
Lam = Struct.new(:expr)
App = Struct.new(:func, :arg)
Literal_int = Struct.new(:value)
Add = Struct.new(:lhs, :rhs)
Let = Struct.new(:binding, :expr)
If = Struct.new(:test, :then_, :else_)

# Runtime values
Closure = Struct.new(:env, :code)
Int_value = Struct.new(:value)

# Instructions
Load_int = Struct.new(:value)
Load_var = Struct.new(:index)
Load_function = Struct.new(:code)
class Add_int end
Branch = Struct.new(:ifNot0, :if0)
class Join end
class Apply end
class Return end

def compile(term, arr)
  case term
  when Literal_int
    arr.push(Load_int.new(term.value))
  when Variable
    arr.push(Load_var.new(term.index))
  when Lam
    code = compile(term.expr, [])
    code.push(Return.new)
    arr.push(Load_function.new(code))
  when App
    compile(term.arg, arr)
    compile(term.func, arr)
    arr.push(Apply.new)
  when Add
    compile(term.rhs, arr)
    compile(term.lhs, arr)
    arr.push(Add.new)
  when Let
    compile(App.new(Lam.new(term.expr), term.binding), arr)
  when If
    compile(term.test, arr)
    arr.push(Branch.new(compile(term.then_, []), compile(term.else_, [])))
  end
  arr
end

# Vm
Dump_tuple = Struct.new(:stack, :env, :control, :pos)
Control_dump_pair = Struct.new(:control, :pos)

class Vm
  def step
    instruction = @control[@pos]
    case instruction
    when Load_int
      @stack.push(Int_value.new(instruction.value))
      @pos += 1
    when Load_var
      @stack.push(@env[instruction.index])
      @pos += 1
    when Branch
      @control_dump.push(Control_dump_pair.new(@control, @pos))
      test = @stack.pop
      @pos = 0
      if test.instance_of?(Int_value) and test.value == 0 then
        @control = instruction.ifNot0
      else
        @control = instruction.if0
      end
    when Join
      pair = @control_dump.pop()
      @control = pair.control
      @pos = pair.pos
    when Load_function
      @stack.push(Closure.new(@env.clone, instruction.code))
      @pos += 1
    when Apply
      @dump.push(Dump_tuple.new(@stack, @env.clone, @control, @pos))
      func = @stack.pop
      arg = @stack.pop
      @stack = []
      @env = func.env.clone
      @env.push(arg)
      @control = func.code
      @pos = 0
    when Return
      ret_val = stack.pop
      to_restore = dump.pop
      @stack = to_restore.stack
      @stack.push(ret_val)
      @control = to_restore.control
      @pos = to_restore.pos
      @env = to_restore.env
    when Add_int
      a = stack.pop
      b = stack.pop
      stack.push(Int_value.new(a.value + b.value))
      @pos += 1
    end
  end

  def execute
    while @pos < @control.length do
      step
    end
    @stack.pop
  end

  def initialize(code)
    @stack = []
    @env = []
    @control = code
    @dump = []
    @control_dump = []
    @pos = 0
  end
end

class Ex1
  def self.run
    term = Let.new(Lam.new(If.new(Variable.new(0), Add.new(Literal_int.new(1), Variable.new(0)), Literal_int.new(0))),
                   App.new(Variable.new(0), App.new(Variable.new(0), Literal_int.new(0))))
    vm = Vm.new(compile(term, []))
    vm.execute
  end
end

Ex1.run
