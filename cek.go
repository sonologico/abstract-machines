package main

import "fmt"

type Term interface{ isTerm() }

type Var struct {
	symbol string
}

type Abs struct {
	param string
	expr  Term
}

type App struct {
	fn  Term
	arg Term
}

func (x Var) isTerm() {}
func (x Abs) isTerm() {}
func (x App) isTerm() {}

type Value interface{ isValue() }

type Env func(string) Value

type Clos struct {
	env   Env
	param string
	expr  Term
}

func (x Clos) isValue() {}

type Cont interface{ isCont() }

type Nothing struct{}

type EvalArg struct {
	arg  Term
	rest Cont
}

type FnApp struct {
	fn   Clos
	rest Cont
}

func (x Nothing) isCont() {}
func (x EvalArg) isCont() {}
func (x FnApp) isCont()   {}

type State struct {
	c Term
	e Env
	k Cont
}

func emptyEnv(s string) Value {
	panic(fmt.Sprintf("invalid var: '%s'", s))
}

func extendEnv(e Env, symbol string, val Value) Env {
	return func(s string) Value {
		if s == symbol {
			return val
		} else {
			return e(s)
		}
	}
}

func step(s State) State {
	switch t := s.c.(type) {
	case Var:
		switch w := s.e(t.symbol).(type) {
		case Clos:
			return State{Abs{w.param, w.expr}, s.e, s.k}
		default:
			return s
		}
	case App:
		return State{t.fn, s.e, EvalArg{t.arg, s.k}}
	case Abs:
		switch k := s.k.(type) {
		case Nothing:
			return s
		case EvalArg:
			return State{k.arg, s.e, FnApp{Clos{s.e, t.param, t.expr}, k.rest}}
		case FnApp:
			return State{k.fn.expr, extendEnv(k.fn.env, k.fn.param, Clos{s.e, t.param, t.expr}), k.rest}
		default:
			return s
		}
	default:
		return s
	}
}

func isTerminal(t Term) bool {
	switch t.(type) {
	case Abs:
		return true
	default:
		return false
	}
}

func multiStep(s State) State {
	if isTerminal(s.c) {
		switch s.k.(type) {
		case Nothing:
			return s
		default:
			return multiStep(step(s))
		}
	} else {
		return multiStep(step(s))
	}
}

func eval(e Env, t Term) Term {
	return multiStep(State{t, e, Nothing{}}).c
}

func show(t Term) string {
	switch x := t.(type) {
	case Var:
		return x.symbol
	case App:
		return fmt.Sprintf("%s(%s)", show(x.fn), show(x.arg))
	case Abs:
		return fmt.Sprintf("(lam %s. %s)", x.param, show(x.expr))
	default:
		return ""
	}
}

func main() {
	tm := App{Abs{"x", Var{"x"}}, Abs{"y", Var{"y"}}}
	fmt.Println(show(tm))
	fmt.Println(show(eval(emptyEnv, tm)))
}
