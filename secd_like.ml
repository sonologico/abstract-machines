(*
 * Syntax
 *)

type term =
  (* x | y | z | ... *)
  | Var of int
  (* fun x -> M *)
  | Fun of term
  (* f x *)
  | App of term * term
  (* ... -2 | -1 | 0 | 1 | 2 | ... *)
  | Int of int
  (* M + N *)
  | Add of term * term
  (* let x = M in N *)
  | Let of term * term
  (* if_not_zero M then N else O *)
  | If of term * term * term


(* example terms *)

(* fun x -> x + x *)
let t1 = Fun (Add (Var 0, Var 0))

(* let f = fun x -> x + x in f 4 *)
let t2 = Let (t1, App (Var 0, Int 4))

(*
 * Vm
 *)

type instruction =
  | Load_int of int
  | Load_var of int
  | Load_function of code
  | Add_int
  | Branch of code * code
  | Join
  | Apply
  | Return

and code = instruction list

type env = vm_value list

and vm_value = Int_value of int | Closure of env * code

type stack = vm_value list

type vm = {
  mutable stack : stack;
  mutable env : env;
  mutable control : code;
  mutable control_dump : code list;
  mutable dump : (stack * env * code) list;
}

let make_vm code =
  { stack = []; env = []; control = code; control_dump = []; dump = [] }

let pop vm =
  match vm.stack with
  | [] -> failwith "stack underflow"
  | x :: xs ->
      vm.stack <- xs;
      x

let step vm =
  match vm.control with
  | Load_int x :: xs ->
      vm.stack <- Int_value x :: vm.stack;
      vm.control <- xs
  | Load_var x :: xs ->
      vm.stack <- List.nth vm.env x :: vm.stack;
      vm.control <- xs
  | Branch (a, b) :: xs -> (
      vm.control_dump <- xs :: vm.control_dump;
      match pop vm with Int_value 0 -> vm.control <- b | _ -> vm.control <- a )
  | [ Join ] -> (
      match vm.control_dump with
      | [] -> assert false
      | x :: xs ->
          vm.control <- x;
          vm.control_dump <- xs )
  | Load_function code :: xs ->
      vm.stack <- Closure (vm.env, code) :: vm.stack;
      vm.control <- xs
  | Apply :: xs -> (
      vm.dump <- (vm.stack, vm.env, xs) :: vm.dump;
      match vm.stack with
      | Closure (env, code) :: arg :: xs ->
          vm.stack <- [];
          vm.control <- code;
          vm.env <- arg :: env
      | _ -> failwith "type error in apply" )
  | [ Return ] -> (
      let ret_val = pop vm in
      match vm.dump with
      | [] -> assert false
      | (stack, env, control) :: rest ->
          vm.dump <- rest;
          vm.stack <- ret_val :: stack;
          vm.control <- control;
          vm.env <- env )
  | Add_int :: xs -> (
      let a = pop vm in
      let b = pop vm in
      match (a, b) with
      | Int_value x, Int_value y ->
          vm.stack <- Int_value (x + y) :: vm.stack;
          vm.control <- xs
      | _ -> failwith "type error in add_int" )
  | _ -> failwith "invalid code"

(* x |> f === f x *)

let rec compile tm list =
  match tm with
  | Int x -> Load_int x :: list
  | Var x -> Load_var x :: list
  | App (f, x) -> Apply :: list |> compile f |> compile x
  | Fun x -> Load_function (compile x [ Return ]) :: list
  | Add (a, b) -> Add_int :: list |> compile a |> compile b
  | Let (binding, x) -> compile (App (Fun x, binding)) list
  | If (test, then_, else_) ->
      Branch (compile then_ [ Join ], compile else_ [ Join ]) :: list
      |> compile test

let rec execute vm =
  match vm.control with
  | [] -> pop vm
  | _ ->
      step vm;
      execute vm

(*
 * Direct interpreter for comparison
 *)

type interpreter_value =
  | Int_value_i of int
  | Closure_i of interpreter_env * term

and interpreter_env = interpreter_value list

let rec eval env tm =
  match tm with
  | Var x -> List.nth env x
  | Int m -> Int_value_i m
  | Add (p, q) -> (
      match (eval env p, eval env q) with
      | Int_value_i m, Int_value_i n -> Int_value_i (m + n)
      | _, _ -> failwith "type error in Add" )
  | Fun e -> Closure_i (env, e)
  | App (f, x) -> (
      match eval env f with
      | Closure_i (env', tm') -> eval (eval env x :: env') tm'
      | _ -> failwith "type error in App" )
  | Let (b, e) -> eval (eval env b :: env) e
  | If (test, then_, else_) -> (
      match eval env test with
      | Int_value_i 0 -> eval env else_
      | _ -> eval env then_ )

(*
 * Example
 *)

let ex1 =
  (* let f = fun x -> if_not_zero x then x + 1 else 0 in f (f 5) *)
  Let
    ( Fun (If (Var 0, Add (Int 1, Var 0), Int 0)),
      App (Var 0, App (Var 0, Int 5)) )

let run () =
  let vm_result = compile ex1 [] |> make_vm |> execute in
  let interpreter_result = eval [] ex1 in
  match (vm_result, interpreter_result) with
  | Int_value x, Int_value_i y when x = y -> Printf.printf "Equal ints: %d\n" x
  | Closure _, Closure_i _ -> print_endline "Both closures"
  | _ -> print_endline "Wrong"
